package question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import ui.HomePage;

public class TheProductsLabel implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        return HomePage.PRODUCT_LABEL.resolveFor(actor).isDisplayed();
    }

    public static Question<Boolean> isDisplayed() {
        return new TheProductsLabel();
    }
}
