package question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import ui.LoginForm;

public class TheErrorMessage implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(LoginForm.ERROR_LABEL).viewedBy(actor).asString();
    }

    public static Question<String> displayed() {
        return new TheErrorMessage();
    }
}
