package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.saucedemo.com/")
public class HomePage extends PageObject {
    public static Target PRODUCT_LABEL = Target.the("products label")
            .locatedBy("#inventory_filter_container .product_label");
    public static Target SORT_SELECT = Target.the("sort dropdown list")
            .locatedBy("#inventory_filter_container > select");
}
