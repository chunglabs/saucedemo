package ui;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginForm {
    public static Target USERNAME_TXT = Target.the("username field")
            .located(By.id("user-name"));
    public static Target PASSWORD_TXT = Target.the("password field")
            .located(By.id("password"));
    public static Target LOGIN_BTN = Target.the("login button")
            .locatedBy("#login_button_container > div > form > input.btn_action");
    public static Target ERROR_LABEL = Target.the("error message label")
            .locatedBy("#login_button_container > div > form > h3");
}
