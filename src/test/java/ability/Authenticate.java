package ability;

import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Ability;

import java.util.List;
import java.util.Map;

public class Authenticate implements Ability {
    private String username;
    private String password;

    public Authenticate(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String Username() {
        return username;
    }

    public String Password() {
        return password;
    }

    public static Authenticate with(DataTable credentialsData){
        List<Map<String,String>> credentials = credentialsData.asMaps(String.class,String.class);
        String user = credentials.get(0).get("username");
        String pass = credentials.get(0).get("password");
        return new Authenticate(user,pass);
    }
    public static Authenticate with(String username, String password){
        return new Authenticate(username, password);
    }
}
