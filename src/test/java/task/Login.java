package task;

import ability.Authenticate;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import ui.LoginForm;

public class Login implements Task {
    public Login(Authenticate authenticate) {
        this.authenticate = authenticate;
    }

    Authenticate authenticate;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(authenticate.Username()).into(LoginForm.USERNAME_TXT),
                Enter.theValue(authenticate.Password()).into(LoginForm.PASSWORD_TXT),
                Click.on(LoginForm.LOGIN_BTN)
        );
    }

    public static Login with(DataTable credentials) {
        return new Login(Authenticate.with(credentials));
    }

    public static Login with(Authenticate authenticate) {
        return new Login(authenticate);
    }
}
