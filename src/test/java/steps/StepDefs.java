package steps;

import ability.Authenticate;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Steps;
import question.TheErrorMessage;
import question.TheProductsLabel;
import task.Login;
import task.OpenHomePage;
import ui.HomePage;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class StepDefs {
    @Steps
    OpenHomePage openHomePage;

    @Given("^that (.*) opened SwagLabs Home Page$")
    public void open_SwagLabs_Home_Page(String actorName) throws Throwable {
        theActorCalled(actorName).attemptsTo(openHomePage);
    }

    @When("^he login with valid account$")
    public void he_log_in_with_valid_account(DataTable credentials) throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                Login.with(credentials)
        );
    }

    @Then("^the products label will be shown$")
    public void the_products_will_be_shown() throws Throwable {
        theActorInTheSpotlight().should(seeThat(
                TheProductsLabel.isDisplayed()
        ));
    }

    @When("^he login with invalid (.*) or (.*)$")
    public void he_login_with_invalide_username_or_password(String username, String password) throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                Login.with(Authenticate.with(username, password))
        );
    }

    @Then("^the error (.*) will be shown$")
    public void the_error_message_will_be_shown(String message) throws Throwable {
        theActorInTheSpotlight().should(
                seeThat(
                        TheErrorMessage.displayed(), equalTo("Epic sadface: " + message)
                )
        );
    }

    @When("^he choose \"([^\"]*)\"$")
    public void he_choose_option(String option) throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                SelectFromOptions.byVisibleText(option).from(HomePage.SORT_SELECT)
        );
    }

    @Then("^the products will be sorted by name descending$")
    public void the_products_will_be_sorted_by_name_descending() throws Throwable {

    }
}
