package steps;

import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class Hooks {
    @Before
    public void set_The_Stage(){
        OnStage.setTheStage(new OnlineCast());
    }
}
