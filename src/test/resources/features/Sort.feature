Feature: Sorting the products

Background:
    Given that Leo opened SwagLabs Home Page
    And he login with valid account
      | username      | password     |
      | standard_user | secret_sauce |

  Scenario: Sorting by name descending
    When he choose "Name (A to Z)"
    Then the products will be sorted by name descending

  Scenario: Sorting by name ascending
    When he choose "Name (Z to A)"
    Then the products will be sorted by name descending

  Scenario: Sorting by price ascending
    When he choose "Price (low to high)"
    Then the products will be sorted by name descending

  Scenario: Sorting by price descending
    When he choose "Price (high to low)"
    Then the products will be sorted by name descending