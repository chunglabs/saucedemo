Feature: Login
  As a customer
  I want to buy some thing
  So that I need to login SwagLabs

  Scenario: Login with valid username and password
    Given that Leo opened SwagLabs Home Page
    When he login with valid account
      | username      | password     |
      | standard_user | secret_sauce |
    Then the products label will be shown

  Scenario Outline: Login with invalid account
    Given that Leo opened SwagLabs Home Page
    When he login with invalid <username> or <password>
    Then the error <message> will be shown
    Examples:
      | username        | password     | message                                                     |
      | locked_out_user | secret_sauce | Sorry, this user has been locked out.                       |
      |                 | secret_sauce | Username is required                                        |
      |                 |              | Username is required                                        |
      | locked_out_user |              | Password is required                                        |
      | wronguser       | secret_sauce | Username and password do not match any user in this service |
      | standard_user   | wrongpass    | Username and password do not match any user in this service |